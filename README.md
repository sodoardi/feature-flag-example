# Feature Flag Example

Naked Laravel Boilerplate with some features that are turned on or off for 
different environments

## Setup
Start the containers using docker
```
docker-compose up -d
```

Port into the application container
```
docker exec -it fte /bin/bash
```

Install dependencies & prepare the environment file 
```
composer install
cp .env.example .env
```

## Usage

Following routes are available:
* localhost:8080/api/active
* localhost:8080/api/active/{name}
* localhost:8080/api/new

Switch APP_ENV in your .env between: local, development, production and 
see the different results in the responses

## How it works
In App/Providers/FeaturesProvider you can turnOn features for different environments 
based on the APP_ENV setting. In different places in the code the Features facade is
used to toggle features (e.g. routes\api.php, App/Providers/AppServiceProvider)

## Just for Fun
Windows users can copy the /git-hooks/windows/pre-commit file to the .git/hooks folder. 
This will trigger execution of Codesniffer, Insights, PHPStan & PHPUnit before the 
commit is applied. If any of the above failes, the commit will not be aborted.
