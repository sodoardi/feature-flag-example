<?php


namespace App\Services;

use YlsIdeas\FeatureFlags\Facades\Features;

class ActiveService implements ActiveServiceInterface
{

    public function reply(): string
    {
        return 'this is from the legacy implementation of the interface';
    }
}
