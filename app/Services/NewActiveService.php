<?php


namespace App\Services;

class NewActiveService implements ActiveServiceInterface
{

    public function reply(): string
    {
        return 'this is from the NEW Service';
    }
}
