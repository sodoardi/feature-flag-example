<?php


namespace App\Services;

interface ActiveServiceInterface
{
    public function reply(): string;
}
