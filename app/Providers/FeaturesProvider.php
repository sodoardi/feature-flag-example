<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use YlsIdeas\FeatureFlags\Facades\Features;

class FeaturesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Features::noBlade();
        Features::noScheduling();
        Features::noValidations();
        Features::noCommands();

        if (App::environment('local')) {
            //toggle features on for local
            Features::turnOn('new_controller');
            Features::turnOn('new_service_class');
            Features::turnOn('new_method_version');
        }

        if (App::environment('development')) {
            //toggle features on for development
            Features::turnOn('new_controller');
        }

        //on for all
        Features::turnOn('include_salutation');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
