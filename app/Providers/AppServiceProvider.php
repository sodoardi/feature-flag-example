<?php

namespace App\Providers;

use App\Services\ActiveService;
use App\Services\ActiveServiceInterface;
use App\Services\NewActiveService;
use Illuminate\Support\ServiceProvider;
use YlsIdeas\FeatureFlags\Facades\Features;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ActiveServiceInterface::class, ActiveService::class);
        if (Features::accessible('new_service_class')) {
            $this->app->bind(ActiveServiceInterface::class, NewActiveService::class);
        }
    }
}
