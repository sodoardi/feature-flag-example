<?php

namespace App\Http\Controllers;

use App\Services\ActiveServiceInterface;
use Illuminate\Http\Request;
use YlsIdeas\FeatureFlags\Facades\Features;

class ActiveController extends Controller
{
    /**
     * @var ActiveServiceInterface
     */
    private $service;

    public function __construct(ActiveServiceInterface $service)
    {

        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response($this->service->reply());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return response('');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (Features::accessible('include_salutation')) {
            return response('hello '.$id);
        }
        return response($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return response('');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return response('');
    }
}
