echo "====================="
echo "Running PHP Code Sniffer"
$cs = docker run --rm -v ${pwd}:/var/www -w /var/www/ php:7.4-cli /bin/bash -c "./vendor/bin/phpcs"

if($?) {
	echo "PHP Code Sniffer succeeded"
} else {
	echo "PHP Code Sniffer failed - aborting commit"
	echo $cs | Out-File ./test-reports/cs.output
	exit 2
}

echo "====================="
echo "Running PHP Insights"
$insights = docker run --rm -v ${pwd}:/var/www -w /var/www/ php:7.4-cli /bin/bash -c "php artisan insights --no-interaction --min-quality=80 --min-complexity=90 --min-architecture=75 --min-style=90"
if($?) {
	echo "PHP Insights succeeded"
} else {
	echo "PHP Insights failed - aborting commit"
	$insights | Out-File ./test-reports/insights.output
	exit 2
}

echo "====================="
echo "Running PHPStan"
$phpstan = docker run --rm -v ${pwd}:/var/www -w /var/www/ php:7.4-cli /bin/bash -c "./vendor/bin/phpstan analyse --no-interaction --configuration ./phpstan.neon"
if($?) {
	echo "PHPStan succeeded"
} else {
	echo "PHPStan failed - aborting commit"
	$phpstan | Out-File ./test-reports/phpstan.output
	exit 2
}

echo "====================="
echo "Running PHPUnit"
$phpunit = docker run --rm -v ${pwd}:/var/www -w /var/www/ mileschou/xdebug:7.4 /bin/ash -c "./vendor/bin/phpunit -d memory_limit=-1"
if($?) {
	echo "PHPUnit succeeded"
} else {
	echo "PHPUnit failed - aborting commit"
	$phpunit | Out-File ./test-reports/phpunit.output
	exit 2
}
echo "====================="
exit 0
